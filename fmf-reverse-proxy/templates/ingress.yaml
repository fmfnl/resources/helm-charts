{{ if .Values.ingress.enabled }}
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: {{ .Release.Name }}
  namespace: {{ .Release.Namespace }}
  labels:
    app: {{ .Release.Name }}

  annotations:
    # Add TLS annotations if desired
    {{- if .Values.ingress.tls.enabled }}
    cert-manager.io/cluster-issuer: {{ .Values.ingress.tls.clusterIssuer }}
    kubernetes.io/tls-acme: "true"
    {{- end }}

    # Add DNS annotations if desired
    {{- if .Values.ingress.dns.enabled }}
    {{- if .Values.ingress.dns.hostnames }}
    external-dns.alpha.kubernetes.io/hostname: {{ .Values.ingress.dns.hostnames | join ", " }}
    {{- else }}
    external-dns.alpha.kubernetes.io/hostname:
      {{- range .Values.ingress.hostnames -}}
        {{- .host | indent 1 }},
      {{- end -}}
    {{- end }}

    # Add DNS target annotation (CNAME) if desired
    {{- with .Values.ingress.dns.cname }}
    external-dns.alpha.kubernetes.io/target: {{ . }}
    {{- end }}
    {{- end }}

    # Add OAuth authorization if desired
    {{- if .Values.ingress.auth.enabled }}
    nginx.ingress.kubernetes.io/auth-response-headers: Authorization
    nginx.ingress.kubernetes.io/auth-signin: {{ .Values.ingress.auth.proxyHost }}/oauth2/start?rd=$scheme%3A%2F%2F$host$escaped_request_uri

    {{- $authZQuery := "?" }}
    {{- with .Values.ingress.auth.allowedRoles }}
    {{- $authZQuery = printf "%sallowed_roles=%s&" $authZQuery (join "," .) }}
    {{- end }}
    {{- with .Values.ingress.auth.allowedGroups }}
    {{- $authZQuery = printf "%sallowed_groups=%s&" $authZQuery (join "," .) }}
    {{- end }}

    nginx.ingress.kubernetes.io/auth-url: {{ .Values.ingress.auth.proxyHost }}/oauth2/auth{{ $authZQuery }}
    {{- end }}

    # Add other annotations if given
    {{- with .Values.ingress.annotations }}
      {{- toYaml . | nindent 4 }}
    {{- end }}

spec:
  rules:
  {{- $relname := .Release.Name }}
  {{- $path := .Values.ingress.defaultPath }}
  {{- range $port := .Values.service.ports }}
  {{- if $port.exposed }}
  {{- range $host := $.Values.ingress.hostnames }}
  - host: {{ $host.host }}
    http:
      paths:
      - pathType: ImplementationSpecific
        path: {{ $host.path | default $path | squote }}
        backend:
          service:
            name: {{ $relname }}
            port:
              number: {{ $port.exposedPort | default $port.port }}
  {{- end }}
  {{- end }}
  {{- end }}

  tls:
  - hosts:
    {{- range .Values.ingress.hostnames }}
    - {{ .host }}
    {{- end}}
    secretName: {{ .Release.Name }}-cert
{{ end }}
